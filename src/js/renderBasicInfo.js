import $ from 'jquery';

export function renderBasicInfo(name, age) {
  $('#name').html(name);
  $('#age').html(age);
}
