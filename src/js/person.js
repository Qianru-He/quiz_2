import { Education } from './education';

export class Person {
  constructor(name, age, description, educations) {
    this.name = name;
    this.age = age;
    this.description = description;
    this.educations = educations.map(item => {
      const { year, title, description } = item;
      return new Education(year, title, description);
    });
  }
}
