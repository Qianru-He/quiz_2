import { renderEducations } from './renderEducations';
import { renderBasicInfo } from './renderBasicInfo';
import { renderAboutMe } from './renderAboutMe';
import { createPerson } from './creatPerson';

const URL = 'http://localhost:3000/person';

function fetchData(url) {
  return fetch(url).then(response => response.json());
}

fetchData(URL)
  .then(response => {
    const { name, age, description, educations } = createPerson(response);
    renderBasicInfo(name, age);
    renderAboutMe(description);
    renderEducations(educations);
  })
  .catch(error => new Error(error));
