import $ from 'jquery';

export function renderEducations(educations) {
  educations.forEach(item => {
    $('#educations').append(`
        <li><span class="year">${item.year}</span><div class="education"><h4>${item.title}</h4>${item.description}</div>`);
  });
}
